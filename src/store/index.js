import Vue from 'vue'
import Vuex from 'vuex'
import scheduledCourses from './scheduledCourses.js'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogged:localStorage.getItem('isLogged') || false,
    isFreePlan:false,
    isFirstLogin: localStorage.getItem('isFirstLogin') || null,
    browserAgent: navigator.userAgent,
    applicationContext:{
      browserAgent: navigator.userAgent,
      isMobile: false,
      isTablet: false,
    }
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    scheduledCourses:scheduledCourses
  }
})
