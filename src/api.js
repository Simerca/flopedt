import axios from 'axios';

import store from '@/store/index';

// axios base default url
axios.defaults.baseURL = 'http://localhost:8000/api';

// Authorization bearer from store
axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;

const getData = res => res.data;

const api = {
    user: {
        get: (id) => axios.get(`/users/${id}`).then(getData),
        update: (id, data) => axios.put(`/users/${id}`, data).then(getData)
    },
    scheduledCourses: {
        get: () => axios.get('/scheduledCourses').then(getData),
        update: (id, data) => axios.put(`/scheduledCourses/${id}`, data).then(getData)
    }
}

export default api;