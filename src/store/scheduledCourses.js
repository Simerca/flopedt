

import scheduledCourses from '@/assets/scheduledCourses.json'
import moment from 'moment'
import _ from 'lodash';

export default {
    namespaced: true,
    state: {
        scheduledCourses: scheduledCourses,
        rollback:[]
    },
    getters: {     
        getScheduledCourses: state => {
            return state.scheduledCourses
        },
        getScheduledCoursesGroupedByGrp: state => {
            let groups = _.groupBy(state.scheduledCourses, 'course.groups[0].name')
            return groups
        },
        getGroups: state => {
            let groups = _.groupBy(state.scheduledCourses, 'course.groups[0].name')
            return Object.keys(groups)
        }
    },
    mutations: {
        ADD_SCHEDULED_COURSE(state, scheduledCourse){
            state.rollback = state.scheduledCourses;
            state.scheduledCourses.push(scheduledCourse)
        },
        SET_SCHEDULED_COURSES(state, scheduledCourses){
            scheduledCourses = scheduledCourses.map(map=>{
                let date = moment().day(map.day).hour(0).minute(0).second(0).millisecond(0)
                date = moment(date).add(map.start_time, 'minutes')
                map.start_time = date.format('YYYY-MM-DD HH:mm')
                return map
            });
            state.scheduledCourses = scheduledCourses
        },
        SET_SCHEDULED_COURSE(state, scheduledCourse){
            let index = _.findIndex(state.scheduledCourses, {id:scheduledCourse.id})
            state.scheduledCourses[index] = scheduledCourse
            return state.scheduledCourses
        },
        ROLLBACK_SCHEDULED_COURSES(state){
            state.scheduledCourses = state.rollback
        }
    },
    actions: {
        addScheduledCourses({commit}, scheduledCourse){
            commit('ADD_SCHEDULED_COURSE', scheduledCourse)
        },
        refreshScheduledCourses({commit}){
            commit('SET_SCHEDULED_COURSES', scheduledCourses)
        },
        editScheduledCourse({commit}, scheduledCourse){
            commit('SET_SCHEDULED_COURSE', scheduledCourse)
        },
        rollbackScheduledCourses({commit}){
            commit('ROLLBACK_SCHEDULED_COURSES');        
        }
    }
            
};