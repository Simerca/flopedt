import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import store from '@/store/index'
// import axios from 'axios'

Vue.use(VueRouter)

const refreshScheduledCourses = () => {
  store.dispatch('scheduledCourses/refreshScheduledCourses')
}

// const isAuth = (to, from, next) => {
//   if(store.state.isLogged){
//     axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;
//     next()
//   }else{
//     next({name:'login'})
//   }
// }

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    beforeEnter: (to, from, next) => {
      refreshScheduledCourses()
      next()
    },
    children: [
      {
        path: '',
        name: 'homeview',
        component: () => import(/* webpackChunkName: "about" */ '@/components/CalendarComponent.vue')
      }
    ]
  },
  {
    path:'/calendrier',
    name:'calendrier',
    component: () => import(/* webpackChunkName: "about" */ '@/views/CalendarView.vue'),
    children: [
      {
        path: '',
        name: 'calendarview',
        component: () => import(/* webpackChunkName: "about" */ '@/components/CalendarComponent.vue')
      }
    ],
    beforeEnter: (to, from, next) => {
      refreshScheduledCourses()
      next()
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/AboutView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
